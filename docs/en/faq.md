# FAQ

## Contributions

### How do I contribute?

[See here.](../contrib)

### I don't know how to program, can I still contribute?

Yes, there's tons of ways to contribute without writing even a single line of code! 
[See here.](../contrib)

### How can I support developement financially?

André, the chief developer, is first and foremost a vegetable farmer getting himself set up. He spent a good portion of 2019 developing Qrop *ex gratia*. In order to help develop and maintain Qrop, you can make a donation via [Liberapay](https://liberapay.com/ah), the free and ethical platform for recurring donations!

## Training

### Is there any forseen software training?

Possibly! Software training is organised by l'Atelier paysan, generally in the autumn/winter. [See their site](https://www.latelierpaysan.org/Formations) to check for training that has been planned.

### I haven't found a training, can I propose one?

Yes, if you think you'll be able to gather up a dozen people. You'll need to contact [the training coordinator](mailto:a.sombardier@latelierpaysan.org?subject=[Qrop] Organisation
 d'une formation) of l'Atelier
paysan.


