# Crop plan

## General presentation

*Crop plan* view lets you edit plantings. A planting is defined by *at least* one species, one variety and sowing, planting, beginning and end of a harvest. The following image presents the state of the view when no plantings are selected:

![Planting map](img/planting-map-fr-02.png)

The following table describes the components of *Planting map* view.

|  Id | Composant                | Description                                 |
| --: | :--------------          | :-------------------------------------------|
|   1 | Add plantings button  | Open a dialog box to add plantings. |
|   2 | Diagram button         | Display or hide the plantings diagram.   |
|   3 | Search bar       | Allows you to filter the plantings of the current season according to diverse criteria: species, variety, duration, etc. |
|  4 | Season spinner | Allows you to navigate between years and seasons. Clicking on the arrows allows you to change seasons. Holding down <kbd>Ctrl</kbd> while clicking changes the year. It's also possible to use the mouse wheel. |
|  5 | Export/import Planting map menu button | Opens a menu allowing you to export the Planting map in PDF or CSV format, or to import in CSV format, or to duplicate to another year. |
|  6 | Notes botton         | Opens the notes and photos side sheet. |
|  7 | plantings checkbox   | Allows you to select plantings           |

The central part of the layout is a table composed of three parts: 

 1. Column indicating the species and variety.
 2. Diagram graphically representing the planting dates, and the beginning and ending of harvest. Les plantingss n'ont pas encore été semées ou
 plantées. Il est possible de cacher ce diagramme en cliquant sur le bouton
 diagramme.
 3. Colonnes donnant plus de détails sur la plantings : durées, rendement, espacement, prix...

### Selecting columns to display

Clicking on the heading of a column sorts the table according that column. Right-clicking on the heading of the table allows you, in the same way, to select the columns to display.

![Select a column](img/header-selection-fr.png)

### Selecting plantings

When one or more plantings is selected by checkboxes, the menu bar changes color, displays the number of plantings selected
[4] and allows you to perform several operations on the plantings:

 1. modify ;
 2. duplicate ;
 3. delete.

![Selected plantings](img/planting-map-fr-03.png)

## Adding plantings

Clicking on the add plantings button opens a dialog box, allowing you to add one or more plantings on the Planting map. The scroll-down menu for selecting the species will apear; then, once the species is chosen, another scroll-down menu will appear, letting you select the variety.

It's possible to add a species by clicking on « Add a species ».
Likewise, modifying families, species, and varieties is possible in the app's [settings](settings).

General appearance of the add plantings window:

![General appearance of the add plantings window](img/planting-form-fr-02.png)

The following table describes the different parts of the add plantings window:

| Id  | Component            | Description                                      |
| --: | :-------------       | :------------------------------------------------|
|  1  | Select species menu | Selecting and adding species. |
|  2  | Select variety menu | Selecting and adding varieties. |
|  3  | Length, spacing, and number of plantings box | Definition of the length (in bed meters) of the plantings, row spacing, number of rows, as well as the number of plantings to create, and of the number of weeks between each planting.|
|  4  | Type of plantings button | Sélection du type de plantings : semis direct, plants faits soi-même ou plants achetés. |
|  5  | Box durées et dates | Définition des durées (selon le type de la plantings) de pépinière (durée entre le semis en motte et la plantation), de croissance et de récoltes. En entrant une date, les autres sont recalculées en fonction des durées renseignées. Il y a deux formats de dates (voir les [paramètres](settings) pour plus de détails). |
|  6  | Box emplacements  | Le bouton de sélection des emplacements n'est visible que lorsqu'une seule plantings doit être crée. Il ouvre une vue du parcellaire décrite ci-après. |
|  7  | Greenhouse box | Définition de la taille des plaques à semis et de la perte estimée en pépinière (calcul du nombre de plaques à semer). Cette boîte n'est visible que lorsque le type de plantings est « plant, fait ». |
|  8  | Box semences | Définition du nombre de graines par trou (que ce soit pour un semis direct ou pour des plants), du pourcentage de semences à ajouter, de la quantité de graines par gramme et calcul de la quantité de semences nécessaire pour chaque plantings. |
|  9  | Box rendements et produits | Définition de l'unité de la plantings (kg, botte, *etc.*), du rendement par mètre de planche et du prix moyen par unité. En renseignant ces champs, le rendement et le chiffre d'affaire escomptés pour chaque plantings s'affichent dans l'entềte de la fenêtre de dialogue. |
| 10  | Boutons mot-clefs | Ajout de mot-clefs à la plantings ; cela permet par la suite de calculer, par exemple, les longueurs de paillage plastique nécessaires pour la saison. |

Lorsque des plantingss sont ajoutées, les tâches de semis et de plantation relatives
sont automatiques créées et apparaissent dans le [calendrier des
tâches](task-calendar).

### Modifying plantingss

Modifying plantings is similar to adding them. Only modified fields will be updated.

## Notes

Clicking on the notes button opens a side sheet on the right side of the app which allows you to visualize notes already taken to which you can add on.

If no plantings are selected it won't be possible to add a note, and therefpre notes for all the season's plantings will be displayed.

![Notes list](img/planting-map-note-01.png)

| Id  | Component            | Description                                      |
| --: | :-------------       | :------------------------------------------------|
|  1  | Note                 | Displays relative information for the note : plantings, the date on which the note was taken, the content. |
| 2   | Photo button       | Allows you to open the photo display for the note. The button remains inactive lest at least one photo is attatched. |
| 3   | Delete button   | Allows you to delete the note. |
| 4   | Close button  | Allows you to close the notes side sheet. |

Si une plantings est sélectionnée, il est alors possible d'ajouter une note, et
d'ajouter une ou plusieurs photos en cliquant sur l'icône « appareil photo ».
Les notes ayant des photos attachées sont signalées par une icône appareil photo
de couleur noire. En cliquant sur cette icône, on peut alors visualiser les
photos attachées à la note :

![Liste des notes, photos](img/planting-map-note-02.png)

## Import/export du Planting map

En cliquant sur le bouton du menu d'import/export du Planting map, un menu apparaît, permettant :

1. d'exporter le Planting map au format PDF pour l'imprimer ;
1. de dupliquer le Planting map pour une autre année ;
1. d'importer le Planting map au format CSV ;
1. d'exporter le Planting map au format CSV.

### Import des données au format CSV

Les données minimales à fournir pour importer un Planting map au format CSV
sont :

| Data  | Header Name |
| :------  | :-------------- |
| Famille  | family |
| Espèce   | crop |
| Fournisseur de semences   | seed_company |
| Variété  | variety |
| Date de semis | sowing_date |
| Date de plantation | planting_date |
| Date de début de récolte | beg_harvest_date |
| Date de fin de récolte | end_harvest_date |

It's also possible to fill in the following data:

| Data  | Header Name |
| :------  | :-------------- |
| Sous serre (0 si non, 1 si oui)| in_greenhouse |
| Longueur en mètres | length	 |
| Nombre de rangs | rows |
| Distance sur le rang | spacing_plants	 |
| Plants nécessaires | plants_needed	 |
| Perte (en pourcentage) | estimated_gh_loss	 |
| Plants à faire | plants_to_start	 |
| Taille de la plaque | tray_size	 |
| Plaques à faire | trays_to_start	 |
| Graines par trou | seeds_per_hole	 |
| Graines par gramme | seeds_per_gram	 |
| Nombre de graines | seeds_number	 |
| Quantité de graines | seeds_quantity	 |
| Pourcentage de graines à commander en plus | seeds_percentage	 |
| Unité | unit	 |
| Rendement par mètre | yield_per_bed_meter	 |
| Prix moyen | average_price |

Lors de l'importation du plan du culture, toutes les informations nécessaires
(famille, espèce, etc) seront ajoutées à la base de données du logiciel.

#### Génération d'un fichier CSV à partir d'un tableur LibreOffice

1. Dans une nouvelle feuille, copier les colonnes du Planting map
correspondant aux données minimales indiquées précédemment.
2. Ajouter une ligne d'en-tête et copier-coller les noms d'entête
correspondant aux colonnes.
3. Dans le menu Fichier, sélectionner « Enregistrer sous... ».
4. Entrer un nom pour le fichier, et sélectionner le format « Texte CSV (.csv)
». Cocher également la case « Éditer les paramètres du filtre ».
5. Cliquer sur « Enregister ».
5. Dans la nouvelle boîte de dialogue, entrer « ; » (point-virgule) comme
séparateur de champs et aucun séparateur de chaîne de caractères.

> **Important.** Make sure that:
>   * the headers are exactly identical to those indicated and do not include any spaces;
>   * all data are filled in for every column (no emplty cells).

## Hotkeys

| Hotkey | In order to                                  |
| --------- | --------------------------------------  |
| <kbd>Ctrl</kbd> + <kbd>F</kbd> | Activate the search bar. |
| <kbd>Ctrl</kbd> + <kbd>N</kbd> | Open the Add Plantings window. |
| <kbd>Ctrl</kbd> + <kbd>A</kbd> | Select all plantings. |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>A</kbd> | Unselect all plantings. |
| <kbd>Ctrl</kbd> + <kbd>E</kbd> | Modify selected plantings. |
| <kbd>Ctrl</kbd> + <kbd>D</kbd> | Duplicate selected plantings. |
| <kbd>Suppr</kbd>               | Delete selected plantings. |
| <kbd>&larr;</kbd> <kbd>&rarr;</kbd> <kbd>&uarr;</kbd> <kbd>&darr;</kbd>| Browse the plantings list. |
| <kbd>Espace</kbd> | (Un)select highlighted plantings. |
| <kbd>Ctrl</kbd> + <kbd>&larr;</kbd> | Go to the previous season. |
| <kbd>Ctrl</kbd> + <kbd>&rarr;</kbd> | Go to the following season. |
| <kbd>Ctrl</kbd> + <kbd>&darr;</kbd> | Go to the previous year. |
| <kbd>Ctrl</kbd> + <kbd>&uarr;</kbd> | Go to the following year. |
