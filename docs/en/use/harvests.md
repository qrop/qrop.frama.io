# Harvests

*Harvests* view allows you to record harvests worked out for each series so that you can know the yield and manhours required for each series. 

![Vue générale du panneau des récoltes](img/harvest-fr-01.png)

Clicking on the « Print » button in the top right-hand corner will open up a dialog box, letting you save the complete list of harvests in PDF format. 

## Adding and editing harvests

Clicking on either the « Add harvests » button, or on an individual harvest opens up a dialog box: 

![Fenêtre d'ajout/édition des récoltes](img/harvest-dialog-fr.png)

While adding a harvest, only the list of series in the process of harvesting will display by default. Clicking on an individual series automatically restricts the list to series of the same species. It's equally possible to display all the series of the Planting Map by unticking the « Current » box.

It is possible to select multiple series. In that case, as many harvests will be created as series selected. The harvest quantity as well as the advised manhours will be equitably distributed amongst each harvest.
