# Installation

## GNU/Linux (AppImage)

Once you have downloaded the AppImage of Qrop, right-click on the file, then click on « Properties ».  Make your way to the « Permissions » tab and tick the « Authorize the execution of the file as a program » box. Close the dialog box and double-click on AppImage to launch Qrop.

!!! note 
    Pour le moment, Qrop n'est disponible pour GNU/Linux que sous la forme
    d'une [AppImage](https://fr.wikipedia.org/wiki/AppImage). Des paquets pour
    distributions seront peut-être un jour disponibles, si des bénévoles
    décident de les maintenir !

## MacOS

Installation <em>via</em> the DMG file.

## Windows

Once you have downloaded the zip-file for the latest version of Qrop corresponding to your architecture (32 or 64 bits), unzip it, open the folder and double-click on the runnable <tt>qrop</tt>.

!!! info "Launch error message"
    Si lors du lancement du programme, un message d'erreur s'affiche, indiquant que le
    fichier MSVCP140.dll ou VCRUNTIME140.dll est absent de votre ordinateur,
    cela signifie que vous devez installer les bibliothèques Runtime Visual C++
    de Microsoft. Pour ce faire, rendez-vous dans le dossier d'installation de
    Qrop (en général, <tt>C:\Program Files\Qrop</tt>), et double-cliquez sur le
    fichier <tt>vc_redist.x86.exe</tt> ou <tt>vc_redist.x64.exe</tt>. Une fois
    l'installation terminée, relancez Qrop.
