# Parcels and crop rotation

*Parcels and crop rotation* view allows for the creation and editing of parcels, as well as the assignment of series to locations.

![General view of the parcels](img/crop-map-fr-01.png)

| Id  | Composant            | Description                                      |
| --: | :-------------       | :------------------------------------------------|
|   1 | Case à cocher d'affichage des séries | Permet d'afficher ou cacher le panneau des séries à affecter |
|   2 | Barre de recherche des séries | Permet de filtrer les séries de la saison en cours selon divers critères : espèce, variété, durée, etc. |
|  3  | Bouton fléché des saison | Permet de naviguer entre les années et les saisons. Les flèches extérieures permettent de changer de d'année, celles intérieures de saison. |
|   4 | Panneau des emplacements |  |
|   5 | Panneau des séries à placer | |
|   6 | Bouton d'édition du parcellaire | Permet l'édition du parcellaire : ajout, duplication et suppression d'emplacements. |
|   7 | Icône de conflit | S'affiche lorsque une séries affichée sur l'emplacement ne respecte pas l'intervalle de rotation de sa famille. Un survol de cet icône permet d'afficher la liste des conflits. |
|   8 | Icône de l'historique  | Afficher l'historique des séries de l'emplacement. |

## Creating and editing parcels

Upon first use, you need to define the parcels. Qrop lets you freely define the parcels regardless of its hierarchy. That way, you can be sure to to have gardens composed of beds, or vice versa with blocks composed of gardens, the latter being composed of beds divided into 6 rows!

To illustrate the point, let's take the former example and define parcels composed of 6 gardens (lettered A to F), themselves composed of 10 beds numbered 1 to 10.

![General vue of parcels](img/crop-map-fr-02.png)

Clicking the button « Add location » [1], will open the following window:

![General vue of parcels](img/crop-map-fr-03.png)

This window allows you to define the following parameters:

| Id | Description |
| :-- | :------------- |
|   1 | Nom de l'emplacement. Il peut être de trois type : (1) numérique (1, 3, 37...); (2) une ou deux lettres ou (3) ou libre. |
|   2 | Longueur de l'emplacement.      |
|   3 | Largeur de l'emplacement.       |
|   4 | Nombre d'emplacements de ce type à créer.      |

While creating many locations, their names are automatically generated depending on the name entered:

 1. numérique : la suite est générée à partir du premier chiffre entre, si l'on entre 3 comme nom et 6 comme nombre, les noms générés seront 3, 4, 5, 6, 7, 8 ;
 2. une ou deux lettres : la suite est générée à partir de la dernière lettre : si l'on entre « SA » comme nom et 6 comme nombre, les noms générés seront SA, SB, SC, SD, SE, SF ;
 3. libre : un chiffre incrémenté est ajouté au nom.

 The example from the preceeding window will give the following result:

![General vue of the parcels](img/crop-map-fr-04.png)

Selecting the locations that we have just created (out gardens), we will be able to add sub-locations (our beds) to them. Clicking on « Add sub-locations », will open the window again. Hence it is sufficient to enter « 1 » as a name and 10 as the number to terminate the definition of our parcels!

![General vue of the parcels](img/crop-map-fr-05.png)

Locations of the first level will then appear on a yellow background which indicates that they have sub-locations. If we would have wanted to add a third level, it would have been necessary to select all locations of the level, and add sub-locations to them.

Each location that has a sub-location will have a different background color according to its level. Locations without sub-locations have a white background. 

In order to leave parcel-editing mode, you need to click on the button [6] again.

## Assignment of series

Series are assigned by dragging the series screen and dropping it in the locations screen. When you hover over the locations screen with a series, two slashes appear indicating the planting dates and the end of harvest for the series. If the cursor indicates a no symbol, that means that there is no more space available for that location. 

Dropping a series onto a location containing sub-locations (for example on a garden and not on a bed), the series will automatically be affect all applicable and necessary sub-locations.

![Add a related location](img/field-map-add-to-parent.gif)

It is equally possible to drag and drop by holding down the 
<kbd>Ctrl</kbd> key in order to assign the series to every location following that into which the series was dropped.

![Add to the following locations](img/field-map-add-to-siblings.gif)


## Hotkeys

| Raccourci | Action                                  |
| --------- | --------------------------------------  |
| <kbd>Ctrl</kbd> + <kbd>F</kbd> | Activer la barre de recherche. |
| <kbd>Ctrl</kbd> + <kbd>&larr;</kbd> | Aller à la saison précédente. |
| <kbd>Ctrl</kbd> + <kbd>&rarr;</kbd> | Aller à la saison suivante. |
| <kbd>Ctrl</kbd> + <kbd>&darr;</kbd> | Aller à l'année précédente. |
| <kbd>Ctrl</kbd> + <kbd>&uarr;</kbd> | Aller à l'année suivante. |
