# Semences et plants

La vue *Semences et plants* est générée automatiquement à partir du plan de
culture. Elle comporte deux listes : celle des semences et celle des plants à
commander. Il est possible de faire une recherche dans les listes, et d'exporter
les listes au format PDF en cliquant sur l'icône « imprimante ».

![Vue des semences et plants à commander](img/seeds-transplants.png)
