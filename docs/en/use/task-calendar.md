# Calendrier des tâches

La vue *Calendrier des tâches* permet le suivi hebdomadaire des différents
tâches à effectuer. Il est possible d'ajouter, de supprimer, de reporter des tâches à la semaine suivante ou précédente, et de marquer les tâches effectuées.

![Vue général du calendrier des tâches](img/task-calendar-fr-01.png)

Le tableau suivant décrit les composants de la vue *Calendrier des tâches*.

|  Id | Composant                | Description                                 |
| --: | :--------------          | :-------------------------------------------|
|   1 | Bouton d'ajout d'une tâche | Ouvre une fenêtre de dialogue d'ajout de tâches. |
|   2 | Barre de recherche de tâches | Permet de filtrer les tâches selon leur type. |
|   3 | Cases à cocher des tâches à afficher | Permet de sélectionner les tâches à afficher : déjà faites, à faire ou en retard. |
|   4 | Bouton fléché de la semaine et de l'année | Sélection de la semaine et de l'année à afficher. Il est possible d'entrer directement ces valeurs en cliquant sur la semaine ou l'année. |
|   5 | Tâche | Représentation de la tâche. Lors du survol avec la souris apparaissent, selon l'état de la tâche, deux boutons de report d'une semaine (-7 et +7) et une bouton de suppression. |
|   6 | Bouton des détails | Permet d'afficher les détails de la tâches lorsqu'elle est affectée à plusieurs séries. |
|   7 | Icône de statut | En cliquant sur cette icône, la tâche est marquée comme étant effectuée aujourd'hui. Une pression prolongée permet d'ouvrir un calendrier pour sélectionner une autre date. |

## Ajout d'une tâche

En cliquant sur le bouton d'ajout de tâches, une fenêtre de dialogue s'ouvre,
permettant d'ajouter une ou plusieurs séries au plan de culture.

![Aspect général de la fenêtre d'ajout de tâche](img/task-form-fr-01.png)

| Id  | Composant            | Description                                      |
| --: | :-------------       | :------------------------------------------------|
|   1 | Menu type | Sélection et ajout du type de tâche : désherbage, irrigation, *etc.*|
|   2 | Bouton tâche effectuée | En cliquant sur ce bouton, on peut marque la tâche comme étant effectuée à ce jour (ou à un autre jour avec un clic prolongé).|
|   3 | Menu méthode | Sélection de la méthode utilisée (par exemple, pour du désherbage, manuel ou mécanique).|
|   4 | Menu outil | Outil utilisé pour effectuer la tâche (herse étrille, vibroplanche, cultibutte...).|
|   5 | Boîte temps et durées | Permet de renseigner la date prévue de la tâche, sa durée en jours et le temps de travail nécessaire pour l'effectuer. Toute tâche ayant une durée en jours supérieure ou égale à 1 apparaîtra sur le parcellaire. Le fait de renseigner le temps de travail permettra de calculer le temps passé sur chaque culture (fonctionnalité à venir). |
|   6 | Boutons séries et emplacements | Permet d'affecter la tâche soit à des séries, soit à des emplacements. |
|   7 | Vue des séries ou emplacements | Selon le type d'affectation choisie, liste les séries ou les emplacements et permet de les sélectionner. |
|   8 | Barre de recherche des séries | Permet de filtrer la liste des séries (par exemple, en tapant « laitue », on ne verra que les séries de laitue). |
|   9 | Case à cocher séries actives | Permet d'afficher uniquement les séries actives à la date prévue pour la tâche. |

L'édition d'une tâche est similaire à la création. Toutefois, il n'est pas possible de modifier une tâche affectée à des séries pour l'affecter à des emplacements, ou inversement.

### Raccourcis clavier, fenêtre d'ajout/édition

| Raccourci | Action                                  |
| --------- | --------------------------------------  |
| <kbd>Ctrl</kbd> + <kbd>A</kbd> | Sélectionner toutes les séries. |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>A</kbd> | Désélectionner toutes les séries. |
| <kbd>Ctrl</kbd> + <kbd>F</kbd> | Activer la barre de recheche des séries. |
| <kbd>Ctrl</kbd> + <kbd>J</kbd> | Afficher uniquement les séries ou cours ou afficher toutes les séries |

## Raccourcis clavier

| Raccourci | Action                                  |
| --------- | --------------------------------------  |
| <kbd>Ctrl</kbd> + <kbd>F</kbd> | Activer la barre de recheche. |
| <kbd>Ctrl</kbd> + <kbd>N</kbd> | Ouvrir la fenêtre d'ajout de tâche. |
| <kbd>E</kbd> | Éditer la tâche en surbrillance. |
| <kbd>D</kbd> | Afficher les détails de la tâche en surbrillance. |
| <kbd>Suppr</kbd> | Supprimer la tâche en surbrillance (si possible).
| <kbd>&larr;</kbd> | Décaler la tâche en surbrillance à la semaine précédente. |
| <kbd>&rarr;</kbd> | Décaler la tâche en surbrillance à la semaine suivante. |
| <kbd>&uarr;</kbd> <kbd>&darr;</kbd>| Naviguer dans la liste des tâches. |
| <kbd>Espace</kbd> | Marquer la tâche en surbrillance comme étant effectuée aujourd'hui. |
| <kbd>Ctrl</kbd> + <kbd>Espace</kbd> | Ouvrir le calendrier pour choisir la date à laquelle la tâche en surbrillance a été effectuée.|
| <kbd>Ctrl</kbd> + <kbd>J</kbd> | Afficher/cacher les séries effectuées. |
| <kbd>Ctrl</kbd> + <kbd>K</kbd> | Afficher/cacher les séries à faire. |
| <kbd>Ctrl</kbd> + <kbd>L</kbd> | Afficher/cacher les séries en retard. |
