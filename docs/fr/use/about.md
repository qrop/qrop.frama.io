---
title: À propos
---

**Qrop** est un logiciel libre de planification et de suivi des cultures en
maraîchage conçu par un groupe de travail de [l'Atelier paysan](https://latelierpaysan.org).

Les fonctionnalités actuelles sont :

* Création et édition du plan de culture.
* Suivi hebdomadaire des différentes tâches à effectuer : semis, plantation,
  désherbage, irrigation...
* Création d'un parcellaire à plusieurs niveaux et affectation des séries aux
  planches en respectant les rotations définies.
* Suivi des récoltes.
* Génération automatique des liste de semences et de plants à commander.

!!! warning "Avertissement" 
    Ce logiciel est pour le moment en phase de développement, il comporte donc des
    instabilités qui peuvent potentiellement aboutir à des pertes de données,
    pour lesquelles ni les membres du groupe de travail « logiciel de
    planification et de suivi des cultures », ni l'Atelier paysan ne sauraient
    être tenu-es pour responsables.



## État d'esprit du groupe de travail

Le groupe de travail outil de suivi de l'activité maraîchère se donne pour
objectif de mettre au point un outil qui facilite la gestion globale de la ferme
maraîchère (de la commande des semences à la vente des légumes). Il s'agit bien
de faciliter le travail du maraîcher et *non de le priver de son savoir-faire
par des automatismes qu'il ne maîtriserait pas*.

Parfaitement conscient de la grande diversité des fermes, nous ferons un effort
particulier sur l'ergonomie pour développer une interface simple et intuitive
mais surtout un outil suffisamment souple pour s'adapter au besoin de chaque
utilisateur⋅ice.

Pour ce faire, nous procéderons selon les étapes suivantes :

1. Recenser les solutions existantes sur les fermes (cahier de culture, fichiers
   excel, logiciels...) et évaluer leur avantages, leurs inconvénients, leurs
   potentiels et leurs limites.
2. Faire l'inventaire des besoins pour la planification et le suivi de
   l'activité de maraichage. Définir les différents scenarii d'usage et
   prioriser les besoins.
3. Rédiger un cahier des charges pour le développement d'un logiciel (délégué à
   un développeur externe)
4. Accompagner le développement en testant le logiciel et en faisant des
   remontées d'utilisateur jusqu'à la validation de la première version stable.
5. Rédiger un mode d'emploi/tutoriel pour la diffusion et l'appropriation de
   l'outil.
6. Diffusion de l'outil et de sa documentation (par exemple outil sous licence
   libre accessible gratuitement sur le [site de l'Atelier
   paysan](https://www.latelierpaysan.org/Plans-et-Tutoriels).
