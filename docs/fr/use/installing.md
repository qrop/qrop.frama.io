# Installation

## Windows

Une fois que vous avez téléchargé l'installeur de la dernière version de Qrop
correspondant à votre architecture (32 ou 64 bits), double-cliquez dessus pour
lancer l'installation. Une fois l'installation terminée, une entrée pour Qrop
sera disponible dans le menu des applications.

!!! info "Message d'erreur au lancement" 
    Si lors du lancement du programme, un
    message d'erreur s'affiche, indiquant que le fichier MSVCP140.dll ou
    VCRUNTIME140.dll est absent de votre ordinateur, cela signifie que vous
    devez installer les bibliothèques Runtime Visual C++ de Microsoft. Pour ce
    faire, rendez-vous dans le dossier d'installation de Qrop (en général,
    `C:\Program Files\Qrop`), et double-cliquez sur le fichier
    `vc_redist.x86.exe` (32 bits) ou `vc_redist.x64.exe` (64 bits). Une fois
    l'installation terminée, relancez Qrop.

    Si le problème persiste, essayez d'installer les exécutables 
    `vc_redist.x86.exe` (32 bits) ou `vc_redist.x64.exe` (64 bits)  depuis 
    [le site de Microsoft](https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads). 

## macOS

Ouvrez le fichier DMG. Faites glisser l'icône de Qrop vers l'icône des applications.

!!! warning "Version minimale"

    Version minimale requise : macOS **10.13**.

    Nous [travaillons](https://framagit.org/ah/qrop/-/issues/173) à faire
    fonctionner Qrop sous macOS 10.12. Les versions inférieures ne pourront
    malheureusement pas être supportées.


## GNU/Linux (AppImage)

Une fois que vous avez téléchargé l'AppImage de Qrop, faites un clic droit sur
le fichier, puis cliquez sur « Propriétés ». Rendez-vous sur l'onglet «
Permissions » et cochez la case « Autoriser l'exécution du fichier comme un
programme ». Fermez la boîte de dialogue et double-cliquez sur l'AppImage pour
lancer Qrop.

Vous pouvez également ouvrir un terminal et taper :

```shell 
chmod u+x Qrop-x86_64.AppImage 
./Qrop-x86_64.AppImage 
```

!!! warning "Version minimale"

    Qrop est compilé sous Ubuntu Trusty (14.04 LTS). Il risque de ne pas fonctionner pour des versions antérieures. 

!!! note 
    Pour le moment, Qrop n'est disponible pour GNU/Linux que sous la forme
    d'une [AppImage](https://fr.wikipedia.org/wiki/AppImage). Des paquets pour
    distributions seront peut-être un jour disponibles, si des bénévoles
    décident de les maintenir !

