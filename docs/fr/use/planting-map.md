# Plan de culture

## Présentation générale

La vue *Plan de culture* permet l’édition des séries de légumes. Une série de
légume est définie *a minima* par une espèce, une variété et des semis,
plantation, début et fin de récolte. L'image suivante présente l'état de la vue
lorsqu'aucune série n'est sélectionnée :

![Plan de culture](img/planting-map-fr-02.png)

Le tableau suivant décrit les composants de la vue *Plan de culture*.

|  Id | Composant                | Description                                 |
| --: | :--------------          | :-------------------------------------------|
|   1 | Bouton d'ajout de série  | Ouvre une fenêtre de dialogue d'ajout des séries. |
|   2 | Bouton diagramme         | Affiche ou cache le diagramme des séries.   |
|   3 | Barre de recherche       | Permet de filtrer les séries de la saison en cours selon divers critères : espèce, variété, durée, etc. |
|  4 | Bouton fléché des saison | Permet de naviguer entre les années et les saisons. Un clic simple sur les flèches permet de changer de saison. En enfonçant la touchant <kbd>Ctrl</kbd> lors du clic, on change alors d'année. Il également possible d'utiliser la molette de la souris. |
|  5 | Bouton du menu d'export/import du plan de culture. | Ouvre un menu permettant d'export le plan de culture au format PDF ou CSV, de l'importer au format CSV ou de le dupliquer à une autre année. |
|  6 | Bouton des notes         | Ouvre le panneau de prise de notes et de photos. |
|  7 | Case à cocher de série   | Permet de sélectionner des séries           |

La partie centrale de la vue est un tableau composé de trois parties :

 1. Colonne indiquant l'espèce et la variété.
 2. Diagramme représentant graphiquement les dates de semis, plantation, début
 de récolte et fin de récolte. Les séries n'ont pas encore été semées ou
 plantées. Il est possible de cacher ce diagramme en cliquant sur le bouton
 diagramme.
 3. Colonnes donnant plus de détails sur la série : durées, rendement, espacement, prix...

### Sélection des colonnes à afficher

En cliquant sur l'entête d'une colonne, on trie le tableau en fonction de cette
colonne. Un clic droit sur l'en-tête du tableau permet également de sélectionner
les colonnes à afficher.

![Sélectione des colonnes](img/header-selection-fr.png)

### Sélection des séries

Lorsqu'une ou plusieurs séries sont sélectionnées au moyen des cases à cocher,
la barre supérieure change de couleur, affiche le nombre de séries sélectionnées
[4] et permet de réaliser plusieurs opérations sur les séries :

 1. éditer ;
 2. dupliquer ;
 3. supprimer.

![Séries sélectionnées](img/planting-map-fr-03.png)

## Ajout de séries

En cliquant sur le bouton d'ajout de séries, une fenêtre de dialogue s'ouvre,
permettant d'ajouter une ou plusieurs séries au plan de culture. Le menu
déroulant de sélection de l'espèce apparaît alors ; puis, une fois l'espèce
choisie, un autre menu déroulant apparaît, permettant de sélectionner la variété.

Il est possible d'ajouter une espèce en cliquant sur « Ajouter une espèce ».
L'édition des familles, espèces et variétés est également possible dans les
[paramètres](settings) de l'application.

Aspect général de la fenêtre d'ajout de séries :

![Aspect général de la fenêtre d'ajout de séries](img/planting-form-fr-02.png)

Le tableau suivant décrit les différents parties de la fenêtre d'ajout de séries :

| Id  | Composant            | Description                                      |
| --: | :-------------       | :------------------------------------------------|
|  1  | Menu de sélection de l'espèce | Sélection et ajout d'espèces. |
|  2  | Menu de sélection de la variété | Sélection et ajout de variétés. |
|  3  | Boîte longueur, distances et nombre de séries | Définition de la longueur (en mètres de planche) de la série, de l'espacement sur le rang, du nombre de rangs, ainsi que du nombre de séries à créer et du nombre de semaines d'intervalle entre chaque série.|
|  4  | Boutons type de série | Sélection du type de série : semis direct, plants faits soi-même ou plants achetés. |
|  5  | Boîte durées et dates | Définition des durées (selon le type de la série) de pépinière (durée entre le semis en motte et la plantation), de croissance et de récoltes. En entrant une date, les autres sont recalculées en fonction des durées renseignées. Il y a deux formats de dates (voir les [paramètres](settings) pour plus de détails). |
|  6  | Boîte emplacements  | Le bouton de sélection des emplacements n'est visible que lorsqu'une seule série doit être crée. Il ouvre une vue du parcellaire décrite ci-après. |
|  7  | Boîte serre | Définition de la taille des plaques à semis et de la perte estimée en pépinière (calcul du nombre de plaques à semer). Cette boîte n'est visible que lorsque le type de série est « plant, fait ». |
|  8  | Boite semences | Définition du nombre de graines par trou (que ce soit pour un semis direct ou pour des plants), du pourcentage de semences à ajouter, de la quantité de graines par gramme et calcul de la quantité de semences nécessaire pour chaque série. |
|  9  | Boîte rendements et produits | Définition de l'unité de la série (kg, botte, *etc.*), du rendement par mètre de planche et du prix moyen par unité. En renseignant ces champs, le rendement et le chiffre d'affaire escomptés pour chaque série s'affichent dans l'entềte de la fenêtre de dialogue. |
| 10  | Boutons mot-clefs | Ajout de mot-clefs à la série ; cela permet par la suite de calculer, par exemple, les longueurs de paillage plastique nécessaires pour la saison. |

Lorsque des séries sont ajoutées, les tâches de semis et de plantation relatives
sont automatiques créées et apparaissent dans le [calendrier des
tâches](task-calendar).

### Édition de séries

L'édition des séries est similaire à l'ajout. Seuls les champs modifiés sont mis
à jour.

## Prise de note

En cliquant sur le bouton de notes, un panneau s'ouvre sur la droite de
l'application permettant de visualiser les notes déjà prises et d'en ajouter.

Si aucune série n'est sélectionnée, il est impossible d'ajouter une note, et
sont alors affichées les notes de toutes les séries de la saison.

![Liste des notes](img/planting-map-note-01.png)

| Id  | Composant            | Description                                      |
| --: | :-------------       | :------------------------------------------------|
|  1  | Note                 | Affiche les informations relatives à la note : la série, la date de la prise de note, le contenu. |
| 2   | Bouton photo         | Permet d'ouvrir le panneau d'affichage des photos de la note. Le bouton n'est actif que s'il y a au moins une photo attachée. |
| 3   | Bouton suppression   | Permet de supprimer la note. |
| 4   | Bouton de fermeture  | Permet de fermer le panneau des notes. |

Si une série est sélectionnée, il est alors possible d'ajouter une note, et
d'ajouter une ou plusieurs photos en cliquant sur l'icône « appareil photo ».
Les notes ayant des photos attachées sont signalées par une icône appareil photo
de couleur noire. En cliquant sur cette icône, on peut alors visualiser les
photos attachées à la note :

![Liste des notes, photos](img/planting-map-note-02.png)

## Import/export du plan de culture

En cliquant sur le bouton du menu d'import/export du plan de culture, un menu apparaît, permettant :

1. d'exporter le plan de culture au format PDF pour l'imprimer ;
1. de dupliquer le plan de culture pour une autre année ;
1. d'importer le plan de culture au format CSV ;
1. d'exporter le plan de culture au format CSV.

### Import des données au format CSV

Les données minimales à fournir pour importer un plan de culture au format CSV
sont :

| Données  | Nom de l'entête |
| :------  | :-------------- |
| Famille  | family |
| Espèce   | crop |
| Fournisseur de semences   | seed_company |
| Variété  | variety |
| Date de semis | sowing_date |
| Date de plantation | planting_date |
| Date de début de récolte | beg_harvest_date |
| Date de fin de récolte | end_harvest_date |

Il est également possible de renseigner les données suivantes :

| Données  | Nom de l'entête |
| :------  | :-------------- |
| Sous serre (0 si non, 1 si oui)| in_greenhouse |
| Longueur en mètres | length	 |
| Nombre de rangs | rows |
| Distance sur le rang | spacing_plants	 |
| Plants nécessaires | plants_needed	 |
| Perte (en pourcentage) | estimated_gh_loss	 |
| Plants à faire | plants_to_start	 |
| Taille de la plaque | tray_size	 |
| Plaques à faire | trays_to_start	 |
| Graines par trou | seeds_per_hole	 |
| Graines par gramme | seeds_per_gram	 |
| Nombre de graines | seeds_number	 |
| Quantité de graines | seeds_quantity	 |
| Pourcentage de graines à commander en plus | seeds_percentage	 |
| Unité | unit	 |
| Rendement par mètre | yield_per_bed_meter	 |
| Prix moyen | average_price |

Lors de l'importation du plan du culture, toutes les informations nécessaires
(famille, espèce, etc) seront ajoutées à la base de données du logiciel.

#### Génération d'un fichier CSV à partir d'un tableur LibreOffice

1. Dans une nouvelle feuille, copier les colonnes du plan de culture
correspondant aux données minimales indiquées précédemment.
2. Ajouter une ligne d'en-tête et copier-coller les noms d'entête
correspondant aux colonnes.
3. Dans le menu Fichier, sélectionner « Enregistrer sous... ».
4. Entrer un nom pour le fichier, et sélectionner le format « Texte CSV (.csv)
». Cocher également la case « Éditer les paramètres du filtre ».
5. Cliquer sur « Enregister ».
5. Dans la nouvelle boîte de dialogue, entrer « ; » (point-virgule) comme
séparateur de champs et aucun séparateur de chaîne de caractères.

> **Important.** Assurez-vous que :
>   * les noms d'entêtes sont exactement ceux indiqués et ne comportent aucun espace ;
>   * toutes les données sont renseignées pour chaque colonne (pas de cellule vide).

## Raccourcis clavier

| Raccourci | Action                                  |
| --------- | --------------------------------------  |
| <kbd>Ctrl</kbd> + <kbd>F</kbd> | Activer la barre de recherche. |
| <kbd>Ctrl</kbd> + <kbd>N</kbd> | Ouvrir la fenêtre d'ajout de séries. |
| <kbd>Ctrl</kbd> + <kbd>A</kbd> | Sélectionner toutes les séries. |
| <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>A</kbd> | Désélectionner toutes les séries. |
| <kbd>Ctrl</kbd> + <kbd>E</kbd> | Éditer les séries sélectionnées. |
| <kbd>Ctrl</kbd> + <kbd>D</kbd> | Dupliquer les séries sélectionnées. |
| <kbd>Suppr</kbd>               | Supprimer les séries sélectionnées. |
| <kbd>&larr;</kbd> <kbd>&rarr;</kbd> <kbd>&uarr;</kbd> <kbd>&darr;</kbd>| Naviguer dans la liste des séries. |
| <kbd>Espace</kbd> | (Dé)sélectionner la série en surbrillance. |
| <kbd>Ctrl</kbd> + <kbd>&larr;</kbd> | Aller à la saison précédente. |
| <kbd>Ctrl</kbd> + <kbd>&rarr;</kbd> | Aller à la saison suivante. |
| <kbd>Ctrl</kbd> + <kbd>&darr;</kbd> | Aller à l'année précédente. |
| <kbd>Ctrl</kbd> + <kbd>&uarr;</kbd> | Aller à l'année suivante. |
